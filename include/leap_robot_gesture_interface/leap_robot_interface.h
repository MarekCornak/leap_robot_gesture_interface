//
// Created by cocohrip on 25. 9. 2020.
//

#ifndef SRC_LEAP_ROBOT_INTERFACE_H
#define SRC_LEAP_ROBOT_INTERFACE_H

#include <ros/ros.h>
#include <std_srvs/Empty.h>
//moveit includes
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>
//transform includes
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
//visual includes
#include <interactive_markers/interactive_marker_server.h>
#include "visualization_msgs/Marker.h"
#include "visualization_msgs/MarkerArray.h"
//services includes
#include <leap_robot_gesture_interface/InsertBox.h>
#include <leap_robot_gesture_interface/Clear.h>
#include <leap_robot_gesture_interface/CalibrateHand.h>
#include <leap_robot_gesture_interface/Mode.h>
#include <leap_robot_gesture_interface/Circle.h>
#include <leap_robot_gesture_interface/CircleDelete.h>
//custom msg includes (leap motion)
#include <leap_motion/Human.h>
#include "leap_motion/Hand.h"
#include "leap_motion/Finger.h"
#include "leap_motion/Bone.h"
#include "Leap.h"


class LeapRobotInteraction{
public:

    //Constructor
    LeapRobotInteraction();

    //Callbacks
    void frameCallback(const leap_motion::Human::ConstPtr& human);
    bool drawCallback(leap_robot_gesture_interface::InsertBox::Request &req, leap_robot_gesture_interface::InsertBox::Response &res);
    bool clearLastCallback(leap_robot_gesture_interface::Clear::Request &req,leap_robot_gesture_interface::Clear::Response &res);
    bool clearCallback(leap_robot_gesture_interface::Clear::Request &req,leap_robot_gesture_interface::Clear::Response &res);
    bool calibrationCallback(leap_robot_gesture_interface::CalibrateHand::Request &req,leap_robot_gesture_interface::CalibrateHand::Response &res);
    bool modeCallback(leap_robot_gesture_interface::Mode::Request &req,leap_robot_gesture_interface::Mode::Response &res);
    bool circleCallback(leap_robot_gesture_interface::Circle::Request &req,leap_robot_gesture_interface::Circle::Response &res);
    bool circleDeleteCallback(leap_robot_gesture_interface::CircleDelete::Request &req,leap_robot_gesture_interface::CircleDelete::Response &res);

    //Custom functions
    void fingerPointInit (const leap_motion::Human::ConstPtr& human, leap_motion::Hand hand, int hnd);
    void pointingTo(geometry_msgs::Point fingerDirection);
    bool pointToObject(geometry_msgs::Point fingerDirection);
    bool pointToCircleMarkerHorizontal();
    void sendToJointState() const;
    void moveRobot() const;
    void createCursor( tf::Stamped<tf::Point> &point, int state);
    void createObjects(std::vector<geometry_msgs::Point> &objectsOrigin, std::vector<geometry_msgs::Point> &objectsSizes);
    void createCircles();
    void deleteCircles();
    void createArrow(tf::Stamped<tf::Point> &point);
    int gesture(leap_motion::Hand hand, int hnd);
    geometry_msgs::Pose setTargetPose(int function);
    void pick(moveit::planning_interface::MoveGroupInterface& move_group);
    void place(moveit::planning_interface::MoveGroupInterface& group);
    void openGripper(trajectory_msgs::JointTrajectory& posture);
    void closedGripper(trajectory_msgs::JointTrajectory& posture);



    //Var Moveit
    const std::string PLANNING_GROUP = "manipulator";
    moveit::planning_interface::MoveGroupInterface *move_group;
    moveit::planning_interface::PlanningSceneInterface planning_scene_interface;
    const robot_state::JointModelGroup* joint_model_group{};
    //std::vector<geometry_msgs::Pose> waypoints;

    geometry_msgs::Point picking_object;
    std::vector<geometry_msgs::Point> gObjectsOrigins; //array of origins of all collison objects
    std::vector<geometry_msgs::Point> gObjectsSizes;  //array of sizes of all collison objects
    std::vector<moveit_msgs::CollisionObject> collision_objects; //array of all collison objects

private:

    //custom var
    bool enable_filter;
    ros::Time tnow;
    bool isCalibrated;
    bool pointing_mode;
    double robot_z;
    double gripper_val;
    bool is_picked;
    double z_diff;

    //hand variable
    leap_motion::Hand userRightHand;
    leap_motion::Hand userLeftHand;
    Leap::Vector palm;
    Leap::Vector rpy;
    Leap::Vector thumbTip;
    Leap::Vector indexTip;
    Leap::Vector middleTip;
    Leap::Vector ringTip;
    Leap::Vector pinkyTip;


    //variables for calibration
    std::vector<double> userPalmCalib;
    std::vector<double> userFistCalib;

    //collision objects variables

    std::vector<std::string> object_ids;
    geometry_msgs::Point gObjectOrigin;
    geometry_msgs::Point gObjectSize;
    int gObject_position;



    //coordinates of intersection on XY plane
    tf::Stamped<tf::Point> intersection;
    tf::Stamped<tf::Point> pointing_origin;
    tf::Stamped<tf::Point> direction;   //direction of the finger
    tf::Stamped<tf::Point> objectIntersection; //intersecton on object plane

    //pub
    ros::Publisher cursor_pub;
    ros::Publisher arrow_pub;
    ros::Publisher circle_pub;

    //subs
    tf::TransformListener tflistener;
    ros::Subscriber human_sub;

    //srv
    ros::ServiceServer draw_service_;
    ros::ServiceServer clear_service_;
    ros::ServiceServer clearLast_service_;
    ros::ServiceServer calibrate_service_;
    ros::ServiceServer mode_service_;
    ros::ServiceServer circle_service_;
    ros::ServiceServer circle_delete_service_;

    //visual var
    visualization_msgs::MarkerArray cursor_marker_array;
    visualization_msgs::Marker cursor_marker;

    visualization_msgs::MarkerArray arrow_marker_array;
    visualization_msgs::Marker arrow_marker;


    visualization_msgs::MarkerArray circle_marker_array;
    visualization_msgs::Marker circle_marker;
    std::vector<geometry_msgs::Point> circle_markers_origins;
    geometry_msgs::Point circle_marker_current;
    std::vector<double> circle_markers_sizes;

};

#endif //SRC_LEAP_ROBOT_INTERFACE_H
