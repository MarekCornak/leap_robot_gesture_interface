//
// Created by cocohrip on 25. 9. 2020.
//

#include "leap_robot_gesture_interface/leap_robot_interface.h"

void addCollisionObjects(moveit::planning_interface::PlanningSceneInterface& planning_scene_interface, LeapRobotInteraction& robot)
{
//    std::vector<moveit_msgs::CollisionObject> collision_objects;
//    collision_objects.resize(1);
//    collision_objects[0].id = "object";
//    collision_objects[0].header.frame_id = "base_link";//"panda_link0";
//
//    /* Define the primitive and its dimensions. */
//    collision_objects[0].primitives.resize(1);
//    collision_objects[0].primitives[0].type = collision_objects[0].primitives[0].BOX;
//    collision_objects[0].primitives[0].dimensions.resize(3);
//    collision_objects[0].primitives[0].dimensions[0] = 0.05;
//    collision_objects[0].primitives[0].dimensions[1] = 0.05;
//    collision_objects[0].primitives[0].dimensions[2] = 0.05;//0.4;
//
//    /* Define the pose of the object. */
//    collision_objects[0].primitive_poses.resize(1);
//    collision_objects[0].primitive_poses[0].position.x = 0.3;
//    collision_objects[0].primitive_poses[0].position.y = 0.4;
//    collision_objects[0].primitive_poses[0].position.z = 0.025;
//    collision_objects[0].operation = collision_objects[0].ADD;
//
//    planning_scene_interface.applyCollisionObjects(collision_objects);
//    robot.picking_object.x = 0.3;
//    robot.picking_object.y = 0.4;
//    robot.picking_object.z = 0.025;

    geometry_msgs::Point objectOrigin;
    geometry_msgs::Point objectSize;

    objectSize.x = 0.05;
    objectSize.y = 0.05;
    objectSize.z = 0.05;

    objectOrigin.x =  0.3;
    objectOrigin.y =  0.4;
    objectOrigin.z =  0.0;

   robot.gObjectsOrigins.push_back(objectOrigin);
   robot.gObjectsSizes.push_back(objectSize);

    robot.createObjects(robot.gObjectsOrigins, robot.gObjectsSizes);

}

int main(int argc, char **argv) {
    ros::init(argc, argv, "leap_robot_interface");
    LeapRobotInteraction robot;

    ros::AsyncSpinner spinner(3);
    spinner.start();

    //addCollisionObjects(robot.planning_scene_interface, robot);
    robot.joint_model_group = robot.move_group->getCurrentState()->getJointModelGroup(robot.PLANNING_GROUP);

    while (ros::ok()) {
        continue;
    }
    ros::shutdown();
    return 0;
}