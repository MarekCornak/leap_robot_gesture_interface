//
// Created by cocohrip on 25. 9. 2020.
//

#include"leap_robot_gesture_interface/leap_robot_interface.h"
#define grip_open_length 0.05
#define hande_maximum_joint_value 0.025 //closed gripper
#define grip_joint_constant 0.5
#define hande_z_length 0.157 //0.146

LeapRobotInteraction::LeapRobotInteraction() {

    ros::NodeHandle nh("leap_motion");

    //leap motion data subscriber
    nh.getParam("/enable_filter", enable_filter);
    if (enable_filter) {
        human_sub = nh.subscribe<leap_motion::Human>("leap_filtered", 1, &LeapRobotInteraction::frameCallback, this);
    } else {
        human_sub = nh.subscribe<leap_motion::Human>("leap_device", 1, &LeapRobotInteraction::frameCallback, this);
    }


    //user services
    draw_service_ = nh.advertiseService("InsertBox", &LeapRobotInteraction::drawCallback, this);
    clear_service_ = nh.advertiseService("clear_all", &LeapRobotInteraction::clearCallback, this);
    clearLast_service_ = nh.advertiseService("clear_last_object", &LeapRobotInteraction::clearLastCallback, this);
    calibrate_service_ = nh.advertiseService("calibrate", &LeapRobotInteraction::calibrationCallback, this);
    mode_service_ = nh.advertiseService("mode", &LeapRobotInteraction::modeCallback, this);
    circle_service_ = nh.advertiseService("circle", &LeapRobotInteraction::circleCallback, this);
    circle_delete_service_ = nh.advertiseService("circle_delete", &LeapRobotInteraction::circleDeleteCallback, this);


    //user publishers
    cursor_pub = nh.advertise<visualization_msgs::MarkerArray>("cursor_marker", 1);
    arrow_pub = nh.advertise<visualization_msgs::MarkerArray>("arrow_marker", 1);
    circle_pub = nh.advertise<visualization_msgs::MarkerArray>("circle_marker", 1);

    //user move group
    move_group = new moveit::planning_interface::MoveGroupInterface(
            PLANNING_GROUP); //planning group (vytvorenie novej planning group)

    intersection.frame_id_ = "world";
    objectIntersection.frame_id_ = "world";

    userPalmCalib = {0.0, 0.0, 0.0, 0.0, 0.0};
    userFistCalib = {0.0, 0.0, 0.0, 0.0, 0.0};
    isCalibrated = false;
    is_picked = false;
    pointing_mode = false;
    robot_z = 0.2;
    z_diff = 0.0;

}

//function that handles service for clearing all objects
bool LeapRobotInteraction::clearCallback(leap_robot_gesture_interface::Clear::Request &req,
                                         leap_robot_gesture_interface::Clear::Response &res) {
    planning_scene_interface.removeCollisionObjects(object_ids);
    object_ids.clear();
    gObjectsOrigins.clear();
    gObjectsSizes.clear();
    collision_objects.clear();
    return true;
}

//function that handles service for clearing the last object
bool LeapRobotInteraction::clearLastCallback(leap_robot_gesture_interface::Clear::Request &req,
                                             leap_robot_gesture_interface::Clear::Response &res) {

    gObjectsSizes.erase(gObjectsSizes.end());
    gObjectsOrigins.erase(gObjectsOrigins.end());
    collision_objects.erase(collision_objects.end());
    planning_scene_interface.removeCollisionObjects(object_ids);
    object_ids.erase(object_ids.end());
    createObjects(gObjectsOrigins, gObjectsSizes);
    return true;

}

//function that handles the service of inserting an object
bool LeapRobotInteraction::drawCallback(leap_robot_gesture_interface::InsertBox::Request &req,
                                        leap_robot_gesture_interface::InsertBox::Response &res) {

    geometry_msgs::Point objectOrigin;
    geometry_msgs::Point objectSize;

    objectSize.x = req.object[0];
    objectSize.y = req.object[1];
    objectSize.z = req.object[2];

    objectOrigin.x = req.object[3];
    objectOrigin.y = req.object[4];
    objectOrigin.z = req.object[5];

    gObjectsOrigins.push_back(objectOrigin);
    gObjectsSizes.push_back(objectSize);

    createObjects(gObjectsOrigins, gObjectsSizes);
    res.success = true;
    return res.success;
}


//TREBA VYRIESIT REDUNDANTNOST KODU (SPRAVENE NARYCHLO)
bool LeapRobotInteraction::calibrationCallback(leap_robot_gesture_interface::CalibrateHand::Request &req,
                                               leap_robot_gesture_interface::CalibrateHand::Response &res) {

    ROS_INFO("Calibrate Hand \n Place open palm above the sensor and press Enter to continue:");
    //std::cout << "Calibrate Hand" << std::endl << "Place open palm above the sensor and press Enter to continue:" << std::endl;
    int calibrate;
    calibrate = std::cin.get();
    if (calibrate == (int) '\n') {
        userPalmCalib.at(0) = palm.distanceTo(thumbTip);
        userPalmCalib.at(1) = palm.distanceTo(indexTip);
        userPalmCalib.at(2) = palm.distanceTo(middleTip);
        userPalmCalib.at(3) = palm.distanceTo(ringTip);
        userPalmCalib.at(4) = palm.distanceTo(pinkyTip);

        ROS_INFO(
                "Calibration successful \n Now, place closed palm (fist) above the sensor and press Enter to continue:");


        //std::cout << "Calibration successful" << std::endl;
        //std::cout << "Now, place closed palm above the sensor and press Enter to continue:" << std::endl;

        calibrate = std::cin.get();
        if (calibrate == (int) '\n') {

            userFistCalib.at(0) = palm.distanceTo(thumbTip);
            userFistCalib.at(1) = palm.distanceTo(indexTip);
            userFistCalib.at(2) = palm.distanceTo(middleTip);
            userFistCalib.at(3) = palm.distanceTo(ringTip);
            userFistCalib.at(4) = palm.distanceTo(pinkyTip);
            // std::cout << "Calibration successful" << std::endl;
            //std::cout << "Calibration completed" << std::endl;
            ROS_INFO("Calibration completed \n Calibration successful");
        }
        isCalibrated = true;

    }
    return true;
}

bool LeapRobotInteraction::modeCallback(leap_robot_gesture_interface::Mode::Request &req,
                                        leap_robot_gesture_interface::Mode::Response &res) {
    if (req.mode == 1) pointing_mode = true;
    else pointing_mode = false;

    return true;
}

bool LeapRobotInteraction::circleCallback(leap_robot_gesture_interface::Circle::Request &req,
                                          leap_robot_gesture_interface::Circle::Response &res) {

    geometry_msgs::Point circleOrigin;
    double circleSize;

    circleOrigin.x = req.object[0];
    circleOrigin.y = req.object[1];
    circleOrigin.z = 0;

    circleSize = req.object[2];

    circle_markers_origins.push_back(circleOrigin);
    circle_markers_sizes.push_back(circleSize);

    createCircles();
    res.success = true;
    return res.success;


    return false;
}

bool LeapRobotInteraction::circleDeleteCallback(leap_robot_gesture_interface::CircleDelete::Request &req,
                                                leap_robot_gesture_interface::CircleDelete::Response &res) {
    deleteCircles();
    return true;
}

// function for getting data about desired fingers from leap motion sensor topic
void LeapRobotInteraction::fingerPointInit(const leap_motion::Human::ConstPtr &human, leap_motion::Hand hand, int hnd) {
    //fingers
//    Leap::Vector indexTip;
//    Leap::Vector thumbTip;
//    Leap::Vector middleTip;
    geometry_msgs::Point indexDirection;

    //palm
//    Leap::Vector palm;
    palm.x = hand.palm_center.x;
    palm.y = hand.palm_center.y;
    palm.z = hand.palm_center.z;

    rpy.x = hand.roll;
    rpy.y = hand.pitch;
    rpy.z = hand.yaw;


    //iterate through fingers data
    leap_motion::Finger finger;
    for (unsigned int j = 0; j < hand.finger_list.size(); j++) {
        finger = hand.finger_list[j];

        //iterate through bones data
        leap_motion::Bone bone;
        for (unsigned int k = 1; k < finger.bone_list.size(); k++) {
            bone = finger.bone_list[k];
            if (finger.type == Leap::Finger::Type::TYPE_INDEX) {
                if (k == Leap::Bone::Type::TYPE_DISTAL) {
                    //set index tip data
                    indexTip.x = bone.bone_end.position.x;
                    indexTip.y = bone.bone_end.position.y;
                    indexTip.z = bone.bone_end.position.z;
                    indexDirection = finger.tipDirection;
                }
            }
            if (finger.type == Leap::Finger::Type::TYPE_THUMB) {
                if (k == Leap::Bone::Type::TYPE_DISTAL) {
                    //set thumb finger tip data
                    thumbTip.x = bone.bone_end.position.x;
                    thumbTip.y = bone.bone_end.position.y;
                    thumbTip.z = bone.bone_end.position.z;
                }
            }
            if (finger.type == Leap::Finger::Type::TYPE_MIDDLE) {
                if (k == Leap::Bone::Type::TYPE_DISTAL) {
                    //set middle finger tip data
                    middleTip.x = bone.bone_end.position.x;
                    middleTip.y = bone.bone_end.position.y;
                    middleTip.z = bone.bone_end.position.z;
                }
            }
            if (finger.type == Leap::Finger::Type::TYPE_RING) {
                if (k == Leap::Bone::Type::TYPE_DISTAL) {
                    //set middle finger tip data
                    ringTip.x = bone.bone_end.position.x;
                    ringTip.y = bone.bone_end.position.y;
                    ringTip.z = bone.bone_end.position.z;
                }
            }
            if (finger.type == Leap::Finger::Type::TYPE_PINKY) {
                if (k == Leap::Bone::Type::TYPE_DISTAL) {
                    //set middle finger tip data
                    pinkyTip.x = bone.bone_end.position.x;
                    pinkyTip.y = bone.bone_end.position.y;
                    pinkyTip.z = bone.bone_end.position.z;
                }
            }
        }
    }


    int g = gesture(hand, hnd);
    if (g == 1) {
        pointingTo(indexDirection);

        if (pointToObject(indexDirection)) {
            createCursor(objectIntersection, 2);
            cursor_pub.publish(cursor_marker_array);
        } else if (pointToCircleMarkerHorizontal()) {
            createCursor(intersection, 4);
            createArrow(intersection);
            cursor_pub.publish(cursor_marker_array);
            arrow_pub.publish(arrow_marker_array);
            //ROS_INFO("pointing to circle");
        } else {
            createCursor(intersection, 1);
            createArrow(intersection);
            cursor_pub.publish(cursor_marker_array);
            arrow_pub.publish(arrow_marker_array);
        }
    } else if (g == 2) {
        pointingTo(indexDirection);
        //condition if user is pointing to object
        if (pointToObject(indexDirection)) {
            createCursor(objectIntersection, 3);
            cursor_pub.publish(cursor_marker_array);
            move_group->setPoseTarget(setTargetPose(2));
            moveRobot(); //move robot to pose

        } else if (pointToCircleMarkerHorizontal()) {
            createCursor(intersection, 3);
            cursor_pub.publish(cursor_marker_array);
            move_group->setPoseTarget(setTargetPose(3));
            moveRobot(); //move robot to pose
        } else {
            createCursor(intersection, 3);
            cursor_pub.publish(cursor_marker_array);
            move_group->setPoseTarget(setTargetPose(1));
            moveRobot(); //move robot to pose


        }
    } else if (g == 3) {
        sendToJointState();
    } else if (g == 6) {
        move_group->setPoseTarget(setTargetPose(4));

        moveRobot(); //move robot to pose
        sleep(1.0);
    } else if (g == 11) {
        if (!is_picked) {
            pick(*move_group);
        }
    } else if (g == 12) {
        if (is_picked) {
            place(*move_group);
        }
    }

}

//function that calculates the intersection point on the given plane
void LeapRobotInteraction::pointingTo(
        geometry_msgs::Point fingerDirection) {
    tf::Point indexTipPoint;
    tf::Point indexDirectionPoint;
    indexTipPoint.setValue(indexTip.x, indexTip.y, indexTip.z);
    indexDirectionPoint.setValue(fingerDirection.x, fingerDirection.y, fingerDirection.z);

    //transformation between frames (monitor -> world)
    tnow = ros::Time::now();
    try {
        tflistener.waitForTransform("/world", "/leap_hands", tnow, ros::Duration(3.0));
    }
    catch (tf::TransformException &ex) {
        ROS_ERROR("%s", ex.what());
        ros::Duration(1.0).sleep();
    }
    tf::Stamped<tf::Point> stampedPointIn;
    tf::Stamped<tf::Point> poseStampedPointOut;
    stampedPointIn.stamp_ = tnow;
    stampedPointIn.frame_id_ = "leap_hands";
    stampedPointIn.setData(indexTipPoint);
    tflistener.transformPoint("/world", stampedPointIn, poseStampedPointOut);
    pointing_origin.setValue(poseStampedPointOut.x(), poseStampedPointOut.y(), poseStampedPointOut.z());

    intersection.setZ(0.0);
    intersection.setX(poseStampedPointOut.x() + fingerDirection.x * (poseStampedPointOut.z() / fingerDirection.y));
    intersection.setY(poseStampedPointOut.y() - fingerDirection.z * (poseStampedPointOut.z() / fingerDirection.y));
    //LeapRobotInteraction::direction = dirStampedPointOut;
}

// function that determines, whether the user is pointing to a horizontal circle marker
bool LeapRobotInteraction::pointToCircleMarkerHorizontal() {

    for (int i = 0; i < circle_markers_sizes.size(); i++) {
        geometry_msgs::Point origin = circle_markers_origins.at(i);
        double radius = circle_markers_sizes[i] / 2;

        if (pow(intersection.x() - origin.x, 2) + pow(intersection.y() - origin.y, 2) < pow(radius, 2)) {

            circle_marker_current.x = origin.x;
            circle_marker_current.y = origin.y;
            return true;
        }
    }

    return false;
}

// function that determines, if the user is pointing to an object
bool LeapRobotInteraction::pointToObject(
        geometry_msgs::Point fingerDirection) {

    geometry_msgs::Point objectOrigin;
    geometry_msgs::Point objectSize;
    int pos = -1;
    int k = 0;
    bool result = false;
    for (auto i = gObjectsOrigins.begin(), j = gObjectsSizes.begin(); i != gObjectsOrigins.end(); i++, j++) {
        objectOrigin = *i;
        objectSize = *j;
        tf::Stamped<tf::Point> tmpIntersection;
        tmpIntersection.frame_id_ = "world";


        //calculation of the intersection
        double n = objectOrigin.y;
        tmpIntersection.setY(n + objectSize.y / 2);
        tmpIntersection.setZ(intersection.z() + fingerDirection.y * ((n - intersection.y()) / fingerDirection.z));
        tmpIntersection.setX(intersection.x() - fingerDirection.x * ((n - intersection.y()) / fingerDirection.z));

        //setting the objectIntersection variable to the right object
        if (abs(tmpIntersection.x()) > abs(objectOrigin.x) - objectSize.x / 2 &&
            abs(tmpIntersection.x()) < abs(objectOrigin.x) + objectSize.x / 2) {
            if (tmpIntersection.z() > objectOrigin.z && tmpIntersection.z() < objectOrigin.z + objectSize.z) {
                if ((objectOrigin.x >= 0 && tmpIntersection.x() >= 0) || (objectOrigin.x <= 0 && tmpIntersection.x() <= 0)) {
                    if (pos == -1) {
                        objectIntersection = tmpIntersection;
                        gObjectSize = objectSize;
                        gObjectOrigin = objectOrigin;
                        pos = k;
                        gObject_position = k;
                        result = true;
                    } else {
                        if ((objectOrigin.x + objectSize.x / 2) > (gObjectOrigin.x + gObjectSize.x / 2)) {
                            objectIntersection = tmpIntersection;
                            gObjectOrigin = objectOrigin;
                            gObjectSize = objectSize;
                            gObject_position = k;
                            result = true;
                        }
                    }
                }
            }
        }
        k++;
    }
    return result;
}

//function of setting the target position of the robot
geometry_msgs::Pose LeapRobotInteraction::setTargetPose(int function) {

    //set pose to intersection on the plane
    if (function == 1) {
        //set target position and orientation (ee_link)
        geometry_msgs::Pose target_pose1;
        target_pose1.position.x = intersection.x();
        target_pose1.position.y = intersection.y();
        if (pointing_mode) target_pose1.position.z = intersection.z() + robot_z;
        else target_pose1.position.z = intersection.z() + 0.2;
        target_pose1.orientation.x = -0.5;
        target_pose1.orientation.y = 0.5;
        target_pose1.orientation.z = 0.5;
        target_pose1.orientation.w = 0.5;
        ROS_INFO("Setting robot pose to intersection at: x = %f, y = %f", target_pose1.position.x,
                 target_pose1.position.y);
        return target_pose1;

        //set pose over the object, which the operator is pointing at
    } else if (function == 2) {
        //set target position and orientation (ee_link)
        geometry_msgs::Pose target_pose1;
        target_pose1.position.x = gObjectOrigin.x;
        target_pose1.position.y = gObjectOrigin.y;
        target_pose1.position.z = gObjectOrigin.z + gObjectSize.z + 0.02 + 0.2;
        target_pose1.orientation.x = -0.5;
        target_pose1.orientation.y = 0.5;
        target_pose1.orientation.z = 0.5;
        target_pose1.orientation.w = 0.5;
        ROS_INFO("Setting robot pose to intersection at: x = %f, y = %f", target_pose1.position.x,
                 target_pose1.position.y);
        return target_pose1;
    } else if (function == 3) {
        //set target position and orientation (ee_link)
        geometry_msgs::Pose target_pose1;
        target_pose1.position.x = circle_marker_current.x;
        target_pose1.position.y = circle_marker_current.y;
        if (pointing_mode) target_pose1.position.z = robot_z;
        else target_pose1.position.z = intersection.z();
        target_pose1.orientation.x = -0.5;
        target_pose1.orientation.y = 0.5;
        target_pose1.orientation.z = 0.5;
        target_pose1.orientation.w = 0.5;
        ROS_INFO("Setting robot pose to intersection at: x = %f, y = %f", target_pose1.position.x,
                 target_pose1.position.y);
        return target_pose1;
    } else if (function == 4) {
        //set target position and orientation (ee_link)
        geometry_msgs::Pose target_pose1 = move_group->getCurrentPose().pose;

        if (pointing_mode) target_pose1.position.z = robot_z;
        else target_pose1.position.z = target_pose1.position.z + z_diff;
        target_pose1.orientation.x = -0.5;
        target_pose1.orientation.y = 0.5;
        target_pose1.orientation.z = 0.5;
        target_pose1.orientation.w = 0.5;
        ROS_INFO("Setting robot pose to intersection at: x = %f, y = %f, z= %f", target_pose1.position.x,
                 target_pose1.position.y, target_pose1.position.z);
        return target_pose1;
    }
}

//function for moving robot
void LeapRobotInteraction::moveRobot() const {

    //plan trajectory and execute
    moveit::planning_interface::MoveGroupInterface::Plan my_plan;

    bool success = (move_group->plan(my_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS);
    ROS_INFO_NAMED("tutorial", "Visualizing plan (pose goal) %s", success ? "" : "FAILED");

    if (success) {
        move_group->move(); //move robot to target
    } else {

    }
}

//function for setting the robot to "up" pose
void LeapRobotInteraction::sendToJointState() const {
    moveit::core::RobotStatePtr current_state = move_group->getCurrentState();
    std::vector<double> joint_group_positions;
    moveit::planning_interface::MoveGroupInterface::Plan my_plan;
    current_state->copyJointGroupPositions(joint_model_group, joint_group_positions);

    std::map<std::string, double> target_pose = move_group->getNamedTargetValues("up");
    auto iterator = target_pose.begin();
    int i = 0;
    while (iterator != target_pose.end()) {
        joint_group_positions[i] = iterator->second;
        iterator++;
        i++;
    }

    bool success = move_group->setJointValueTarget(joint_group_positions);
    move_group->plan(my_plan);
    ROS_INFO_NAMED("tutorial", "Visualizing plan (joint space goal) %s", success ? "" : "FAILED");

    if (success) {
        move_group->move();
    }
}

//function for creating cursor marker for monitor
void LeapRobotInteraction::createCursor(tf::Stamped<tf::Point> &point, int state) {

    cursor_marker.header.frame_id = "world";
    cursor_marker.header.stamp = tnow;
    cursor_marker.ns = "index_cursor";
    cursor_marker.id = 111;
    cursor_marker.type = visualization_msgs::Marker::SPHERE;
    cursor_marker.action = visualization_msgs::Marker::MODIFY;

    cursor_marker.pose.position.x = point.x();
    cursor_marker.pose.position.y = point.y();
    cursor_marker.pose.position.z = point.z();

    cursor_marker.scale.x = 0.015;
    cursor_marker.scale.y = 0.015;
    cursor_marker.scale.z = 0.015;

    if (state == 1) {
        //red cursor: user is pointing to the table
        cursor_marker.color.r = 1.0f;
        cursor_marker.color.g = 0.0f;
        cursor_marker.color.b = 0.0f;
        cursor_marker.color.a = 1.0f;
        cursor_marker.lifetime = ros::Duration(0.1);
    } else if (state == 2) {
        //blue curor: user is pointing to an object
        cursor_marker.color.r = 0.0f;
        cursor_marker.color.g = 0.0f;
        cursor_marker.color.b = 1.0f;
        cursor_marker.color.a = 1.0f;
        cursor_marker.lifetime = ros::Duration(0.1);
    } else if (state == 3) {
        //user made gesture for moving robot to certain pose
        cursor_marker.color.r = 0.0f;
        cursor_marker.color.g = 1.0f;
        cursor_marker.color.b = 0.0f;
        cursor_marker.color.a = 1.0f;
        cursor_marker.lifetime = ros::Duration(1.0);
    } else if (state == 4) {
        //yellow curor: user is pointing to a horizontal circle marker
        cursor_marker.color.r = 1.0f;
        cursor_marker.color.g = 1.0f;
        cursor_marker.color.b = 0.0f;
        cursor_marker.color.a = 1.0f;
        cursor_marker.lifetime = ros::Duration(0.1);
    }

    cursor_marker_array.markers.push_back(cursor_marker);

    if (pointing_mode) {
        cursor_marker.ns = "floating_cursor";
        cursor_marker.id = 112;
        cursor_marker.pose.position.z = point.z() + robot_z;

        cursor_marker.color.r = 1.0f;
        cursor_marker.color.g = 0.0f;
        cursor_marker.color.b = 0.0f;
        cursor_marker.color.a = 1.0f;
        if (state == 3) cursor_marker.lifetime = ros::Duration(1.0);
        else cursor_marker.lifetime = ros::Duration(0.1);
        cursor_marker_array.markers.push_back(cursor_marker);
    }
}


void LeapRobotInteraction::createArrow(tf::Stamped<tf::Point> &point) {

    arrow_marker.header.frame_id = "world";
    arrow_marker.header.stamp = tnow;
    arrow_marker.ns = "arrow_cursor";
    arrow_marker.id = 222;
    arrow_marker.type = visualization_msgs::Marker::ARROW;


    arrow_marker.action = visualization_msgs::Marker::MODIFY;

    arrow_marker.points.resize(2);

    arrow_marker.points[0].x = pointing_origin.x();
    arrow_marker.points[0].y = pointing_origin.y();
    arrow_marker.points[0].z = pointing_origin.z();

    arrow_marker.points[1].x = point.x();
    arrow_marker.points[1].y = point.y();
    arrow_marker.points[1].z = point.z();


    arrow_marker.scale.x = 0.005;
    arrow_marker.scale.y = 0.01;
    arrow_marker.scale.z = 0.02;

    //red cursor: user is pointing to the table
    arrow_marker.color.r = 1.0f;
    arrow_marker.color.g = 1.0f;
    arrow_marker.color.b = 0.0f;
    arrow_marker.color.a = 1.0f;
    arrow_marker.lifetime = ros::Duration(0.1);

    //ROS_INFO("Arrow start: %f , %f ,%f, Arrow end: %f , %f ,%f",pointing_origin.x(),pointing_origin.y(),pointing_origin.z(),intersection.x() ,intersection.y(), intersection.z() );

    arrow_marker_array.markers.push_back(arrow_marker);


    if (pointing_mode) {

        arrow_marker.ns = "arrow_cursor";
        arrow_marker.id = 223;
        arrow_marker.action = visualization_msgs::Marker::ADD;

        arrow_marker.points[1].x = intersection.x();
        arrow_marker.points[1].y = intersection.y();
        arrow_marker.points[1].z = intersection.z();

        arrow_marker.points[1].x = point.x();
        arrow_marker.points[1].y = point.y();
        arrow_marker.points[1].z = point.z() + robot_z;

        arrow_marker.scale.x = 0.005;
        arrow_marker.scale.y = 0.01;
        arrow_marker.scale.z = 0.02;

        //red cursor: user is pointing to the table
        arrow_marker.color.r = 1.0f;
        arrow_marker.color.g = 1.0f;
        arrow_marker.color.b = 0.0f;
        arrow_marker.color.a = 1.0f;
        arrow_marker.lifetime = ros::Duration(0.1);

        arrow_marker_array.markers.push_back(arrow_marker);
    } else{
        arrow_marker.ns = "arrow_cursor";
        arrow_marker.id = 223;
        arrow_marker.action = visualization_msgs::Marker::MODIFY;

        arrow_marker.points[1].x = point.x();
        arrow_marker.points[1].y = point.y();
        arrow_marker.points[1].z = point.z();

        arrow_marker.points[0].x = point.x();
        arrow_marker.points[0].y = point.y();
        arrow_marker.points[0].z = point.z() + robot_z;

        arrow_marker.scale.x = 0.005;
        arrow_marker.scale.y = 0.01;
        arrow_marker.scale.z = 0.02;

        //red cursor: user is pointing to the table
        arrow_marker.color.r = 1.0f;
        arrow_marker.color.g = 1.0f;
        arrow_marker.color.b = 0.0f;
        arrow_marker.color.a = 1.0f;
        arrow_marker.lifetime = ros::Duration(0.1);

        arrow_marker_array.markers.push_back(arrow_marker);
    }
}

void LeapRobotInteraction::deleteCircles() {
    for (int i = 0, j; i < circle_markers_sizes.size(); i++) {
        int id = i + 1;
        //create object
        circle_marker.header.frame_id = "world";
        circle_marker.header.stamp = tnow;
        circle_marker.ns = "circle_cursor";
        circle_marker.id = id;
        circle_marker.type = visualization_msgs::Marker::CYLINDER;
        circle_marker.action = visualization_msgs::Marker::DELETEALL;
        circle_marker_array.markers.push_back(circle_marker);
    }
    circle_pub.publish(circle_marker_array);
}


void LeapRobotInteraction::createCircles() {

    for (int i = 0; i < circle_markers_sizes.size(); i++) {
        int id = i + 1;
        geometry_msgs::Point circle_origin = circle_markers_origins.at(i);
        double size = circle_markers_sizes.at(i);
        //create object
        circle_marker.header.frame_id = "world";
        circle_marker.header.stamp = tnow;
        circle_marker.ns = "circle_cursor";
        circle_marker.id = id;
        circle_marker.type = visualization_msgs::Marker::CYLINDER;
        circle_marker.action = visualization_msgs::Marker::ADD;

        circle_marker.pose.position.x = circle_origin.x;
        circle_marker.pose.position.y = circle_origin.y;
        circle_marker.pose.position.z = circle_origin.z;

        circle_marker.scale.x = size;
        circle_marker.scale.y = size;
        circle_marker.scale.z = 0;

        //red cursor: user is pointing to the table
        circle_marker.color.r = 1.0f;
        circle_marker.color.g = 0.0f;
        circle_marker.color.b = 0.0f;
        circle_marker.color.a = 1.0f;
        circle_marker.lifetime = ros::Duration(0);
        circle_marker_array.markers.push_back(circle_marker);
        ROS_INFO("Adding circle marker at: X = %f, Y=%f, Z =%f Size = %f", circle_origin.x, circle_origin.y,
                 circle_origin.z, size);
    }
    circle_pub.publish(circle_marker_array);
}


//function for creating objects
void LeapRobotInteraction::createObjects(std::vector<geometry_msgs::Point> &objectsOrigins,
                                         std::vector<geometry_msgs::Point> &objectsSizes) {
    geometry_msgs::Point objectOrigin;
    geometry_msgs::Point objectSize;

    //int index = 1;
    for (int i = 0, j; i < objectsOrigins.size(); i++) {
        std::string id;
        id = std::to_string(i + 1);
        objectOrigin = objectsOrigins.at(i);
        objectSize = objectsSizes.at(i);
        //create object
        moveit_msgs::CollisionObject collision_object;
        collision_object.header.frame_id = "base_link"; //move_group->getPlanningFrame();
        collision_object.id = id;


        shape_msgs::SolidPrimitive primitive;
        primitive.type = primitive.BOX;
        primitive.dimensions.resize(3);
        primitive.dimensions[0] = objectSize.x;
        primitive.dimensions[1] = objectSize.y;
        primitive.dimensions[2] = objectSize.z;

        geometry_msgs::Pose box_pose;
        box_pose.orientation.w = 1.0;
        box_pose.position.x = objectOrigin.x;
        box_pose.position.y = objectOrigin.y;
        box_pose.position.z = objectOrigin.z + objectSize.z / 2;

        collision_object.primitives.push_back(primitive);
        collision_object.primitive_poses.push_back(box_pose);
        collision_object.operation = collision_object.ADD;
        object_ids.push_back(collision_object.id);


        collision_objects.push_back(collision_object);
        ROS_INFO_NAMED("tutorial", "Add an object into the world");
        planning_scene_interface.addCollisionObjects(collision_objects);
        // index++;
    }
}

int LeapRobotInteraction::gesture(leap_motion::Hand hand, int hnd) {
    double offsetL = 0.01;
    double offsetH = 0.02;

    double distThumb = palm.distanceTo(thumbTip);
    double distIndex = palm.distanceTo(indexTip);
    double distMiddle = palm.distanceTo(middleTip);
    double distRing = palm.distanceTo(ringTip);
    double distPinky = palm.distanceTo(pinkyTip);

    if (isCalibrated) {

        //pointing gesture
        if ((distThumb + offsetH >= userFistCalib.at(0) && distThumb - offsetH <= userFistCalib.at(0)) &&
            (distIndex + offsetL >= userPalmCalib.at(1) && distIndex - offsetL <= userPalmCalib.at(1)) &&
            (distMiddle + offsetH >= userFistCalib.at(2) && distMiddle - offsetH <= userFistCalib.at(2)) &&
            (distRing + offsetH >= userFistCalib.at(3) && distRing - offsetH <= userFistCalib.at(3)) &&
            (distPinky + offsetH >= userFistCalib.at(4) && distPinky - offsetH <= userFistCalib.at(4) &&
            hnd == 1)
                ) {
            // std::cout << "pointing" << std::endl;
            return 1;
        }
            //pointing gestre with thumb up
        else if ((distThumb + offsetL >= userPalmCalib.at(0) && distThumb - offsetL <= userPalmCalib.at(0)) &&
                 (distIndex + offsetL >= userPalmCalib.at(1) && distIndex - offsetL <= userPalmCalib.at(1)) &&
                 (distMiddle + offsetH >= userFistCalib.at(2) && distMiddle - offsetH <= userFistCalib.at(2)) &&
                 (distRing + offsetH >= userFistCalib.at(3) && distRing - offsetH <= userFistCalib.at(3)) &&
                 (distPinky + offsetH >= userFistCalib.at(4) && distPinky - offsetH <= userFistCalib.at(4)&&
                  hnd == 1)
                ) {
            // std::cout << "pointing to move" << std::endl;
            return 2;
        }
            //thumb up gesture
        else if ((distThumb + offsetL >= userPalmCalib.at(0) && distThumb - offsetL <= userPalmCalib.at(0)) &&
                 (distIndex + offsetH >= userFistCalib.at(1) && distIndex - offsetH <= userFistCalib.at(1)) &&
                 (distMiddle + offsetH >= userFistCalib.at(2) && distMiddle - offsetH <= userFistCalib.at(2)) &&
                 (distRing + offsetH >= userFistCalib.at(3) && distRing - offsetH <= userFistCalib.at(3) &&
                                                               hnd == 1)
                ) {
            if (rpy.x >= 0.6) {
                if (robot_z >= 0.06) robot_z = robot_z - 0.02;
                z_diff = -0.02;
                ROS_INFO("moving robot down z = %f",robot_z);
                return 6;
            } else if (rpy.x <= -0.6) {
                if (robot_z <= 0.38)robot_z = robot_z + 0.02;
                z_diff = 0.02;
                ROS_INFO("moving robot up z = %f",robot_z);
                return 6;
            }
            //std::cout << "thumb up" << std::endl;
            return 4;

            //Rocker gesture - home gesture
        } else if ((distThumb + offsetH >= userFistCalib.at(0) && distThumb - offsetH <= userFistCalib.at(0)) &&
                   (distIndex + offsetL >= userPalmCalib.at(1) && distIndex - offsetL <= userPalmCalib.at(1)) &&
                   (distMiddle + offsetH >= userFistCalib.at(2) && distMiddle - offsetH <= userFistCalib.at(2)) &&
                   (distRing + offsetH >= userFistCalib.at(3) && distRing - offsetH <= userFistCalib.at(3)) &&
                   (distPinky + offsetH >= userPalmCalib.at(4) && distPinky - offsetH <= userPalmCalib.at(4) &&
                                                                  hnd == 1)
                ) {
            //std::cout << "go home - calibrated" << std::endl;
            return 5;
            //Pinky Up gesture
        } else if ((distThumb + offsetH >= userFistCalib.at(0) && distThumb - offsetH <= userFistCalib.at(0)) &&
                   (distIndex + offsetH >= userFistCalib.at(1) && distIndex - offsetH <= userFistCalib.at(1)) &&
                   (distMiddle + offsetH >= userFistCalib.at(2) && distMiddle - offsetH <= userFistCalib.at(2)) &&
                   (distRing + offsetH >= userFistCalib.at(3) && distRing - offsetH <= userFistCalib.at(3)) &&
                   (distPinky + offsetH >= userPalmCalib.at(4) && distPinky - offsetH <= userPalmCalib.at(4) &&
                                                                  hnd == 1)
                ) {
            return 3;
        }
    } else {
        //pointing
        if (palm.distanceTo(indexTip) > palm.distanceTo(thumbTip) &&
            palm.distanceTo(indexTip) > 1.5 * palm.distanceTo(ringTip) &&
            palm.distanceTo(indexTip) > 2 * palm.distanceTo(pinkyTip) &&
            hnd == 1
                ) {
//            std::cout << "pointing" << std::endl;
            return 1;
        }
        //pointing to move
        if (palm.distanceTo(indexTip) > 1.5 * palm.distanceTo(ringTip) &&
            palm.distanceTo(thumbTip) > 2.275 * palm.distanceTo(ringTip) &&
            hnd == 1) {
//            std::cout << "pointing to move" << std::endl;
            return 2;

        }
        //rocker
        if (palm.distanceTo(indexTip) > palm.distanceTo(thumbTip) &&
            palm.distanceTo(indexTip) > 1.5 * palm.distanceTo(ringTip) &&
            palm.distanceTo(pinkyTip) > 1.5 * palm.distanceTo(ringTip) &&
            hnd == 1) {
            //std::cout << "go home" << std::endl;
            return 5;

        }
        //pinky up
        if (palm.distanceTo(pinkyTip) > 1.5 * palm.distanceTo(ringTip) &&
            palm.distanceTo(pinkyTip) > 1.25 * palm.distanceTo(indexTip) &&
            hnd == 1) {
            return 3;

        }
        //thumb up
        if (palm.distanceTo(thumbTip) > 1.5 * palm.distanceTo(ringTip) &&
            palm.distanceTo(thumbTip) > 2 * palm.distanceTo(indexTip) &&
            hnd == 1) {
//            std::cout << "go home" << std::endl;
            //ROS_INFO("roll: %f", rpy.x);
            if (rpy.x >= 0.6) {
                if (robot_z >= 0.06) robot_z = robot_z - 0.02;
                z_diff = -0.02;
                ROS_INFO("moving robot down z = %f",robot_z);
                return 6;
            } else if (rpy.x <= -0.6) {
                if (robot_z <= 0.38)robot_z = robot_z + 0.02;
                z_diff = 0.02;
                ROS_INFO("moving robot up z = %f",robot_z);
                return 6;
            }
            return 4;
        }
        if (hnd == 0){
            //ROS_INFO("Sphere radius: %f", hand.sphere_radius);
            if (hand.sphere_radius < 0.05){
                //ROS_INFO("Closed");
                return 11; //hand closed
            } else if (hand.sphere_radius > 0.17){
                //ROS_INFO("Opened");
                return 12; //hand opened
            }


        } else return 0;

    }

}

//function handling the data from leap motion sensor
void LeapRobotInteraction::frameCallback(const leap_motion::Human::ConstPtr &human) {

    leap_motion::Hand hand;
    tf::StampedTransform transform;
    if (human->right_hand.is_present) {
        hand = human->right_hand;
        userRightHand = hand;
        LeapRobotInteraction::fingerPointInit(human, hand,1);

    }

    //functionality is programed for left hand
    if (human->left_hand.is_present) {
        hand = human->left_hand;
        userLeftHand = hand;
        LeapRobotInteraction::fingerPointInit(human, hand,0);
    }
}

void LeapRobotInteraction::pick(moveit::planning_interface::MoveGroupInterface &move_group) {
    // Create a vector of grasps to be attempted, currently only creating single grasp.
    // This is essentially useful when using a grasp generator to generate and test multiple grasps.
    std::vector<moveit_msgs::Grasp> grasps;
    grasps.resize(1);

    grasps[0].grasp_pose.header.frame_id = "base_link";
    tf2::Quaternion orientation;
    //orientation.setRPY(-M_PI / 2, -M_PI / 4, -M_PI / 2);
    orientation.setValue(-0.5, 0.5, 0.5, 0.5);
    grasps[0].grasp_pose.pose.orientation = tf2::toMsg(orientation);
    grasps[0].grasp_pose.pose.position.x = gObjectOrigin.x;
    grasps[0].grasp_pose.pose.position.y = gObjectOrigin.y;
    grasps[0].grasp_pose.pose.position.z = gObjectOrigin.z + hande_z_length;

    // Setting pre-grasp approach
    // ++++++++++++++++++++++++++
    /* Defined with respect to frame_id */
    grasps[0].pre_grasp_approach.direction.header.frame_id = "base_link";
    /* Direction is set as positive x axis */
    grasps[0].pre_grasp_approach.direction.vector.z = -1.0;
    grasps[0].pre_grasp_approach.min_distance = 0.1;
    grasps[0].pre_grasp_approach.desired_distance = 0.115;

    // Setting post-grasp retreat
    // ++++++++++++++++++++++++++
    /* Defined with respect to frame_id */
    grasps[0].post_grasp_retreat.direction.header.frame_id = "base_link";
    /* Direction is set as positive z axis */
    grasps[0].post_grasp_retreat.direction.vector.z = 1.0;
    grasps[0].post_grasp_retreat.min_distance = 0.1;
    grasps[0].post_grasp_retreat.desired_distance = 0.115;

    // Setting posture of ee_link before grasp
    // +++++++++++++++++++++++++++++++++++
    openGripper(grasps[0].pre_grasp_posture);
    // END_SUB_TUTORIAL

    // Setting posture of ee_link during grasp
    // +++++++++++++++++++++++++++++++++++
    closedGripper(grasps[0].grasp_posture);

    // Set support surface as table1.
    move_group.setSupportSurfaceName("table");
    // Call pick to pick up the object using the grasps given
   if (move_group.pick(std::to_string(gObject_position+1), grasps)){is_picked = true;}
}

void LeapRobotInteraction::place(moveit::planning_interface::MoveGroupInterface &group) {

    // Create a vector of placings to be attempted, currently only creating single place location.
    std::vector<moveit_msgs::PlaceLocation> place_location;
    place_location.resize(1);

    // Setting place location pose
    // +++++++++++++++++++++++++++
    place_location[0].place_pose.header.frame_id = "base_link";
    tf2::Quaternion orientation;
    orientation.setValue(0,0,0,1);
    place_location[0].place_pose.pose.orientation = tf2::toMsg(orientation);

    if (pointToCircleMarkerHorizontal()){
        place_location[0].place_pose.pose.position.x = circle_marker_current.x;
        place_location[0].place_pose.pose.position.y = circle_marker_current.y;
        place_location[0].place_pose.pose.position.z = gObjectOrigin.z + gObjectSize.z/2;
    } else {
        /* While placing it is the exact location of the center of the object. */
        place_location[0].place_pose.pose.position.x = intersection.x();
        place_location[0].place_pose.pose.position.y = intersection.y();
        place_location[0].place_pose.pose.position.z = gObjectOrigin.z + gObjectSize.z/2;
    }

    // Setting pre-place approach
    // ++++++++++++++++++++++++++
    /* Defined with respect to frame_id */
    place_location[0].pre_place_approach.direction.header.frame_id = "base_link";
    /* Direction is set as negative z axis */
    place_location[0].pre_place_approach.direction.vector.z = -1.0;
    place_location[0].pre_place_approach.min_distance = 0.01;
    place_location[0].pre_place_approach.desired_distance = 0.115;

    // Setting post-grasp retreat
    // ++++++++++++++++++++++++++
    /* Defined with respect to frame_id */
    place_location[0].post_place_retreat.direction.header.frame_id = "base_link";
    /* Direction is set as negative y axis */
    place_location[0].post_place_retreat.direction.vector.z = 1.0;
    place_location[0].post_place_retreat.min_distance = 0.1;
    place_location[0].post_place_retreat.desired_distance = 0.115;

    // Setting posture of eef after placing object
    // +++++++++++++++++++++++++++++++++++++++++++
    /* Similar to the pick case */
    openGripper(place_location[0].post_place_posture);

    // Set support surface as table.
    group.setSupportSurfaceName("table");
    // Call place to place the object using the place locations given.
    if (group.place(std::to_string(gObject_position+1), place_location)){is_picked = false;
        //update object position
        if (pointToCircleMarkerHorizontal()){
            gObjectsOrigins[gObject_position].x = circle_marker_current.x;
            gObjectsOrigins[gObject_position].y = circle_marker_current.y;
        } else {
            gObjectsOrigins[gObject_position].x = intersection.x();
            gObjectsOrigins[gObject_position].y = intersection.y();
        }

    }

}

void LeapRobotInteraction::openGripper(trajectory_msgs::JointTrajectory &posture) {
    /* Add finger joint. */
    posture.joint_names.resize(1);
    posture.joint_names[0] = "hande_joint_finger";
    /* Set them as open, wide enough for the object to fit. */
    posture.points.resize(1);
    posture.points[0].positions.resize(1);
    posture.points[0].positions[0] = 0.00;
    posture.points[0].time_from_start = ros::Duration(0.5);
}

void LeapRobotInteraction::closedGripper(trajectory_msgs::JointTrajectory &posture) {
    /* Add finger joint. */
    posture.joint_names.resize(1);
    posture.joint_names[0] = "hande_joint_finger";
    /* Set them as closed. */
    posture.points.resize(1);
    posture.points[0].positions.resize(1);
    posture.points[0].positions[0] = (grip_open_length - gObjectSize.x) * grip_joint_constant;
    posture.points[0].time_from_start = ros::Duration(0.5);
}


