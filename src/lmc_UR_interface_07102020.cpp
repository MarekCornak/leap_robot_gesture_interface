//
// Created by cocohrip on 25. 9. 2020.
//

#include"leap_robot_gesture_interface/leap_robot_interface.h"

LeapRobotInteraction::LeapRobotInteraction() {

    ros::NodeHandle nh("leap_motion");

    //leap motion data subscriber
    nh.getParam("/enable_filter", enable_filter);
    if (enable_filter) {
        human_sub = nh.subscribe<leap_motion::Human>("leap_filtered", 1, &LeapRobotInteraction::frameCallback, this);
    } else {
        human_sub = nh.subscribe<leap_motion::Human>("leap_device", 1, &LeapRobotInteraction::frameCallback, this);
    }


    //user services
    draw_service_ = nh.advertiseService("InsertBox", &LeapRobotInteraction::drawCallback, this);
    clear_service_ = nh.advertiseService("clear_all", &LeapRobotInteraction::clearCallback, this);
    clearLast_service_ = nh.advertiseService("clear_last_object", &LeapRobotInteraction::clearLastCallback, this);
    calibrate_service_ = nh.advertiseService("calibrate", &LeapRobotInteraction::calibrationCallback, this);
    mode_service_ = nh.advertiseService("mode", &LeapRobotInteraction::modeCallback, this);

    //user publishers
    cursor_pub = nh.advertise<visualization_msgs::MarkerArray>("cursor_marker", 1);
    arrow_pub = nh.advertise<visualization_msgs::MarkerArray>("arrow_marker", 1);

    //user move group
    move_group = new moveit::planning_interface::MoveGroupInterface(
            PLANNING_GROUP); //planning group (vytvorenie novej planning group)

    intersection.frame_id_ = "world";
    objectIntersection.frame_id_ = "world";

    userPalmCalib = {0.0, 0.0, 0.0, 0.0, 0.0};
    userFistCalib = {0.0, 0.0, 0.0, 0.0, 0.0};
    isCalibrated = false;
    pointing_mode = false;
    robot_z = 0.2;
}

//function that handles service for clearing all objects
bool LeapRobotInteraction::clearCallback(leap_robot_gesture_interface::Clear::Request &req,
                                         leap_robot_gesture_interface::Clear::Response &res) {
    planning_scene_interface.removeCollisionObjects(object_ids);
    object_ids.clear();
    gObjectsOrigins.clear();
    gObjectsSizes.clear();
    collision_objects.clear();
    return true;
}

//function that handles service for clearing the last object
bool LeapRobotInteraction::clearLastCallback(leap_robot_gesture_interface::Clear::Request &req,
                                             leap_robot_gesture_interface::Clear::Response &res) {

    gObjectsSizes.erase(gObjectsSizes.end());
    gObjectsOrigins.erase(gObjectsOrigins.end());
    collision_objects.erase(collision_objects.end());
    planning_scene_interface.removeCollisionObjects(object_ids);
    object_ids.erase(object_ids.end());
    createObjects(gObjectsOrigins, gObjectsSizes);
    return true;

}

//function that handles the service of inserting an object
bool LeapRobotInteraction::drawCallback(leap_robot_gesture_interface::InsertBox::Request &req,
                                        leap_robot_gesture_interface::InsertBox::Response &res) {

    geometry_msgs::Point objectOrigin;
    geometry_msgs::Point objectSize;

    objectSize.x = req.object[0];
    objectSize.y = req.object[1];
    objectSize.z = req.object[2];

    objectOrigin.x = req.object[3];
    objectOrigin.y = req.object[4];
    objectOrigin.z = req.object[5];

    gObjectsOrigins.push_back(objectOrigin);
    gObjectsSizes.push_back(objectSize);

    createObjects(gObjectsOrigins, gObjectsSizes);
    res.success = true;
    return res.success;
}


//TREBA VYRIESIT REDUNDANTNOST KODU (SPRAVENE NARYCHLO)
bool LeapRobotInteraction::calibrationCallback(leap_robot_gesture_interface::CalibrateHand::Request &req,
                                               leap_robot_gesture_interface::CalibrateHand::Response &res) {

    ROS_INFO("Calibrate Hand \n Place open palm above the sensor and press Enter to continue:");
    //std::cout << "Calibrate Hand" << std::endl << "Place open palm above the sensor and press Enter to continue:" << std::endl;
    int calibrate;
    calibrate = std::cin.get();
    if (calibrate == (int) '\n') {

        userPalmCalib.at(0) = palm.distanceTo(thumbTip);
        userPalmCalib.at(1) = palm.distanceTo(indexTip);
        userPalmCalib.at(2) = palm.distanceTo(middleTip);
        userPalmCalib.at(3) = palm.distanceTo(ringTip);
        userPalmCalib.at(4) = palm.distanceTo(pinkyTip);

        ROS_INFO(
                "Calibration successful \n Now, place closed palm (fist) above the sensor and press Enter to continue:");


        //std::cout << "Calibration successful" << std::endl;
        //std::cout << "Now, place closed palm above the sensor and press Enter to continue:" << std::endl;

        calibrate = std::cin.get();
        if (calibrate == (int) '\n') {

            userFistCalib.at(0) = palm.distanceTo(thumbTip);
            userFistCalib.at(1) = palm.distanceTo(indexTip);
            userFistCalib.at(2) = palm.distanceTo(middleTip);
            userFistCalib.at(3) = palm.distanceTo(ringTip);
            userFistCalib.at(4) = palm.distanceTo(pinkyTip);
            // std::cout << "Calibration successful" << std::endl;
            //std::cout << "Calibration completed" << std::endl;
            ROS_INFO("Calibration completed \n Calibration successful");
        }
        isCalibrated = true;

    }
    return true;
}

bool LeapRobotInteraction::modeCallback(leap_robot_gesture_interface::Mode::Request &req,
                                        leap_robot_gesture_interface::Mode::Response &res) {
    if (req.mode == 1) pointing_mode = true;
    else pointing_mode = false;

    return true;
}

// function for getting data about desired fingers from leap motion sensor topic
void LeapRobotInteraction::fingerPointInit(const leap_motion::Human::ConstPtr &human, leap_motion::Hand hand) {
    //fingers
//    Leap::Vector indexTip;
//    Leap::Vector thumbTip;
//    Leap::Vector middleTip;
    geometry_msgs::Point indexDirection;

    //palm
//    Leap::Vector palm;
    palm.x = hand.palm_center.x;
    palm.y = hand.palm_center.y;
    palm.z = hand.palm_center.z;

    //iterate through fingers data
    leap_motion::Finger finger;
    for (unsigned int j = 0; j < hand.finger_list.size(); j++) {
        finger = hand.finger_list[j];

        //iterate through bones data
        leap_motion::Bone bone;
        for (unsigned int k = 1; k < finger.bone_list.size(); k++) {
            bone = finger.bone_list[k];
            if (finger.type == Leap::Finger::Type::TYPE_INDEX) {
                if (k == Leap::Bone::Type::TYPE_DISTAL) {
                    //set index tip data
                    indexTip.x = bone.bone_end.position.x;
                    indexTip.y = bone.bone_end.position.y;
                    indexTip.z = bone.bone_end.position.z;
                    indexDirection = finger.tipDirection;
                }
            }
            if (finger.type == Leap::Finger::Type::TYPE_THUMB) {
                if (k == Leap::Bone::Type::TYPE_DISTAL) {
                    //set thumb finger tip data
                    thumbTip.x = bone.bone_end.position.x;
                    thumbTip.y = bone.bone_end.position.y;
                    thumbTip.z = bone.bone_end.position.z;
                }
            }
            if (finger.type == Leap::Finger::Type::TYPE_MIDDLE) {
                if (k == Leap::Bone::Type::TYPE_DISTAL) {
                    //set middle finger tip data
                    middleTip.x = bone.bone_end.position.x;
                    middleTip.y = bone.bone_end.position.y;
                    middleTip.z = bone.bone_end.position.z;
                }
            }
            if (finger.type == Leap::Finger::Type::TYPE_RING) {
                if (k == Leap::Bone::Type::TYPE_DISTAL) {
                    //set middle finger tip data
                    ringTip.x = bone.bone_end.position.x;
                    ringTip.y = bone.bone_end.position.y;
                    ringTip.z = bone.bone_end.position.z;
                }
            }
            if (finger.type == Leap::Finger::Type::TYPE_PINKY) {
                if (k == Leap::Bone::Type::TYPE_DISTAL) {
                    //set middle finger tip data
                    pinkyTip.x = bone.bone_end.position.x;
                    pinkyTip.y = bone.bone_end.position.y;
                    pinkyTip.z = bone.bone_end.position.z;
                }
            }
        }
    }

    gesture();

    // hand init gesture condition: (fist with index finger straight)
//    if (palm.distanceTo(indexTip) > palm.distanceTo(thumbTip) &&
//        palm.distanceTo(indexTip) > 1.5 * palm.distanceTo(ringTip)) {
//
//        pointingTo(indexDirection);
//
//        //hand gesture condition for setting the target pose for robot (fist with index finger and thumb straight (L-shape))
//        if (palm.distanceTo(indexTip) > 1.5 * palm.distanceTo(ringTip) &&
//            palm.distanceTo(thumbTip) > 2.275 * palm.distanceTo(ringTip)) {
//
//            //condition if user is pointing to object
//            if (pointToObject(indexDirection)) {
//                createCursor(objectIntersection, 3);
//                cursor_pub.publish(cursor_marker_array);
//                move_group->setPoseTarget(setTargetPose(2));
//                moveRobot(); //move robot to pose
//
//            } else {
//                createCursor(intersection, 3);
//                cursor_pub.publish(cursor_marker_array);
//                move_group->setPoseTarget(setTargetPose(1));
//                moveRobot(); //move robot to pose
//
//            }
//
//        } else {
//            if (pointToObject(indexDirection)) {
//                createCursor(objectIntersection, 2);
//                cursor_pub.publish(cursor_marker_array);
//            } else {
//                createCursor(intersection, 1);
//                cursor_pub.publish(cursor_marker_array);
//            }
//        }
//    } else if (palm.distanceTo(thumbTip) > 1.5 * palm.distanceTo(ringTip) &&
//               palm.distanceTo(thumbTip) > 2 * palm.distanceTo(indexTip)) {
//        sendToJointState();
//    }

    if (gesture() == 1) {
        pointingTo(indexDirection);

        if (pointToObject(indexDirection)) {
            createCursor(objectIntersection, 2);
            cursor_pub.publish(cursor_marker_array);
        } else {
            createCursor(intersection, 1);
            createArrow();
            cursor_pub.publish(cursor_marker_array);
            arrow_pub.publish(arrow_marker_array);
        }
    } else if (gesture() == 2) {
        pointingTo(indexDirection);
        //condition if user is pointing to object
        if (pointToObject(indexDirection)) {
            createCursor(objectIntersection, 3);
            cursor_pub.publish(cursor_marker_array);
            move_group->setPoseTarget(setTargetPose(2));
            moveRobot(); //move robot to pose

        } else {
            createCursor(intersection, 3);
            cursor_pub.publish(cursor_marker_array);
            move_group->setPoseTarget(setTargetPose(1));
            moveRobot(); //move robot to pose

        }
    } else if (gesture() == 3) {
        sendToJointState();
    }

}

//function that calculates the intersection point on the given plane
void LeapRobotInteraction::pointingTo(
        geometry_msgs::Point fingerDirection) {
    tf::Point indexTipPoint;
    tf::Point indexDirectionPoint;
    indexTipPoint.setValue(indexTip.x, indexTip.y, indexTip.z);
    indexDirectionPoint.setValue(fingerDirection.x, fingerDirection.y, fingerDirection.z);

    //transformation between frames (monitor -> world)
    tnow = ros::Time::now();
    try {
        tflistener.waitForTransform("/world", "/leap_hands", tnow, ros::Duration(3.0));
    }
    catch (tf::TransformException &ex) {
        ROS_ERROR("%s", ex.what());
        ros::Duration(1.0).sleep();
    }
    tf::Stamped<tf::Point> stampedPointIn;
    tf::Stamped<tf::Point> poseStampedPointOut;
    stampedPointIn.stamp_ = tnow;
    stampedPointIn.frame_id_ = "leap_hands";
    stampedPointIn.setData(indexTipPoint);
    tflistener.transformPoint("/world", stampedPointIn, poseStampedPointOut);
    pointing_origin.setValue(poseStampedPointOut.x(), poseStampedPointOut.y(), poseStampedPointOut.z());

    intersection.setZ(0.0);
    intersection.setX(poseStampedPointOut.x() + fingerDirection.x * (poseStampedPointOut.z() / fingerDirection.y));
    intersection.setY(poseStampedPointOut.y() - fingerDirection.z * (poseStampedPointOut.z() / fingerDirection.y));
    //LeapRobotInteraction::direction = dirStampedPointOut;
}

// function that determines, if the user is pointing to an object
bool LeapRobotInteraction::pointToObject(
        geometry_msgs::Point fingerDirection) {

    geometry_msgs::Point objectOrigin;
    geometry_msgs::Point objectSize;
    int pos = -1;
    int k = 0;
    bool result = false;
    for (auto i = gObjectsOrigins.begin(), j = gObjectsSizes.begin(); i != gObjectsOrigins.end(); i++, j++) {
        objectOrigin = *i;
        objectSize = *j;
        tf::Stamped<tf::Point> tmpIntersection;
        tmpIntersection.frame_id_ = "world";


        //calculation of the intersection
        double n = objectOrigin.y;
        tmpIntersection.setY(n + objectSize.y / 2);
        tmpIntersection.setZ(intersection.z() + fingerDirection.y * ((n - intersection.y()) / fingerDirection.z));
        tmpIntersection.setX(intersection.x() - fingerDirection.x * ((n - intersection.y()) / fingerDirection.z));

        //setting the objectIntersection variable to the right object
        if (abs(tmpIntersection.x()) > abs(objectOrigin.x) - objectSize.x / 2 &&
            abs(tmpIntersection.x()) < abs(objectOrigin.x) + objectSize.x / 2) {
            if (tmpIntersection.z() > objectOrigin.z && tmpIntersection.z() < objectOrigin.z + objectSize.z) {

                if (pos == -1) {
                    objectIntersection = tmpIntersection;
                    gObjectSize = objectSize;
                    gObjectOrigin = objectOrigin;
                    pos = k;
                    result = true;
                } else {
                    if ((objectOrigin.x + objectSize.x / 2) > (gObjectOrigin.x + gObjectSize.x / 2)) {
                        objectIntersection = tmpIntersection;
                        gObjectOrigin = objectOrigin;
                        gObjectSize = objectSize;
                        result = true;
                    }
                }

            }
        }
        k++;
    }
    return result;
}

//function of setting the target position of the robot
geometry_msgs::Pose LeapRobotInteraction::setTargetPose(int function) {

    //set pose to intersection on the plane
    if (function == 1) {
        //set target position and orientation (ee_link)
        geometry_msgs::Pose target_pose1;
        target_pose1.position.x = intersection.x();
        target_pose1.position.y = intersection.y();
        if (pointing_mode) target_pose1.position.z = intersection.z() + robot_z;
        else target_pose1.position.z = intersection.z();
        target_pose1.orientation.x = 0.0;
        target_pose1.orientation.y = 0.7071068;
        target_pose1.orientation.z = 0;
        target_pose1.orientation.w = 0.7071068;
        ROS_INFO("Setting robot pose to intersection at: x = %f, y = %f", target_pose1.position.x,
                 target_pose1.position.y);
        return target_pose1;

        //set pose over the object, which the operator is pointing at
    } else if (function == 2) {
        //set target position and orientation (ee_link)
        geometry_msgs::Pose target_pose1;
        target_pose1.position.x = gObjectOrigin.x;
        target_pose1.position.y = gObjectOrigin.y;
        target_pose1.position.z = gObjectOrigin.z + gObjectSize.z + 0.02;
        target_pose1.orientation.x = 0.0;
        target_pose1.orientation.y = 0.7071068;
        target_pose1.orientation.z = 0;
        target_pose1.orientation.w = 0.7071068;
        ROS_INFO("Setting robot pose to intersection at: x = %f, y = %f", target_pose1.position.x,
                 target_pose1.position.y);
        return target_pose1;
    }
}

//function for moving robot
void LeapRobotInteraction::moveRobot() {

    //plan trajectory and execute
    moveit::planning_interface::MoveGroupInterface::Plan my_plan;

    bool success = (move_group->plan(my_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS);
    ROS_INFO_NAMED("tutorial", "Visualizing plan (pose goal) %s", success ? "" : "FAILED");

    if (success) {
        move_group->move(); //move robot to target
    } else if (!success) {

    }
}

//function for setting the robot to "up" pose
void LeapRobotInteraction::sendToJointState() {
    moveit::core::RobotStatePtr current_state = move_group->getCurrentState();
    std::vector<double> joint_group_positions;
    moveit::planning_interface::MoveGroupInterface::Plan my_plan;
    current_state->copyJointGroupPositions(joint_model_group, joint_group_positions);
//    joint_group_positions[0] = 0.0;
//    joint_group_positions[1] = -1.5707;
//    joint_group_positions[2] = 0.006;
//    joint_group_positions[3] = -1.5707;
//    joint_group_positions[4] = 0.0;
//    joint_group_positions[5] = 0.003;

//    std::map target_pose = move_group->getNamedTargetValues("up");
    std::map<std::string, double> target_pose = move_group->getNamedTargetValues("up");
    auto iterator = target_pose.begin();
    int i = 0;
    while (iterator != target_pose.end()) {
        joint_group_positions[i] = iterator->second;
        iterator++;
        i++;
    }

    bool success = move_group->setJointValueTarget(joint_group_positions);
    move_group->plan(my_plan);
    ROS_INFO_NAMED("tutorial", "Visualizing plan (joint space goal) %s", success ? "" : "FAILED");

    if (success) {
        move_group->move();
    }
}

//function for creating cursor marker for monitor
void LeapRobotInteraction::createCursor(const tf::Stamped<tf::Point> point, int state) {
    cursor_marker.header.frame_id = "world";
    cursor_marker.header.stamp = tnow;
    cursor_marker.ns = "index_cursor";
    cursor_marker.id = 111;
    cursor_marker.type = visualization_msgs::Marker::SPHERE;
    cursor_marker.action = visualization_msgs::Marker::ADD;

    cursor_marker.pose.position.x = point.x();
    cursor_marker.pose.position.y = point.y();
    cursor_marker.pose.position.z = point.z();

    cursor_marker.scale.x = 0.015;
    cursor_marker.scale.y = 0.015;
    cursor_marker.scale.z = 0.015;

    if (state == 1) {
        //red cursor: user is pointing to the table
        cursor_marker.color.r = 1.0f;
        cursor_marker.color.g = 0.0f;
        cursor_marker.color.b = 0.0f;
        cursor_marker.color.a = 1.0f;
        cursor_marker.lifetime = ros::Duration(0.1);
    } else if (state == 2) {
        //blue curor: user is pointing to an object
        cursor_marker.color.r = 0.0f;
        cursor_marker.color.g = 0.0f;
        cursor_marker.color.b = 1.0f;
        cursor_marker.color.a = 1.0f;
        cursor_marker.lifetime = ros::Duration(0.1);
    } else if (state == 3) {
        //user made gesture for moving robot to certain pose
        cursor_marker.color.r = 0.0f;
        cursor_marker.color.g = 1.0f;
        cursor_marker.color.b = 0.0f;
        cursor_marker.color.a = 1.0f;
        cursor_marker.lifetime = ros::Duration(1.0);
    }

    cursor_marker_array.markers.push_back(cursor_marker);

    if (pointing_mode) {
        cursor_marker.ns = "floating_cursor";
        cursor_marker.id = 112;
        cursor_marker.pose.position.z = point.z() + robot_z;

        cursor_marker.color.r = 1.0f;
        cursor_marker.color.g = 0.0f;
        cursor_marker.color.b = 0.0f;
        cursor_marker.color.a = 1.0f;
        if (state == 3) cursor_marker.lifetime = ros::Duration(1.0);
        else cursor_marker.lifetime = ros::Duration(0.1);
        cursor_marker_array.markers.push_back(cursor_marker);
    }
}


void LeapRobotInteraction::createArrow() {

    arrow_marker.header.frame_id = "world";
    arrow_marker.header.stamp = tnow;
    arrow_marker.ns = "arrow_cursor";
    arrow_marker.id = 222;
    arrow_marker.type = visualization_msgs::Marker::ARROW;
//    if (arr) {
//        arrow_marker.action = visualization_msgs::Marker::MODIFY;
//
//    } else {
//        arrow_marker.action = visualization_msgs::Marker::ADD;
//        ROS_WARN("ADDING ARROW");
//        arr = true;
//    }

    arrow_marker.action = visualization_msgs::Marker::ADD;

    arrow_marker.points.resize(2);

    arrow_marker.points[0].x = pointing_origin.x();
    arrow_marker.points[0].y = pointing_origin.y();
    arrow_marker.points[0].z = pointing_origin.z();

    arrow_marker.points[1].x = intersection.x();
    arrow_marker.points[1].y = intersection.y();
    arrow_marker.points[1].z = intersection.z();


    arrow_marker.scale.x = 0.005;
    arrow_marker.scale.y = 0.01;
    arrow_marker.scale.z = 0.02;

    //red cursor: user is pointing to the table
    arrow_marker.color.r = 1.0f;
    arrow_marker.color.g = 1.0f;
    arrow_marker.color.b = 0.0f;
    arrow_marker.color.a = 1.0f;
    arrow_marker.lifetime = ros::Duration(0.1);

    //ROS_INFO("Arrow start: %f , %f ,%f, Arrow end: %f , %f ,%f",pointing_origin.x(),pointing_origin.y(),pointing_origin.z(),intersection.x() ,intersection.y(), intersection.z() );

    arrow_marker_array.markers.push_back(arrow_marker);


    if (pointing_mode) {

        arrow_marker.ns = "arrow_cursor";
        arrow_marker.id = 223;
        arrow_marker.action = visualization_msgs::Marker::ADD;

        arrow_marker.points[1].x = intersection.x();
        arrow_marker.points[1].y = intersection.y();
        arrow_marker.points[1].z = intersection.z();

        arrow_marker.points[0].x = intersection.x();
        arrow_marker.points[0].y = intersection.y();
        arrow_marker.points[0].z = intersection.z() + robot_z;

        arrow_marker.scale.x = 0.005;
        arrow_marker.scale.y = 0.01;
        arrow_marker.scale.z = 0.02;

        //red cursor: user is pointing to the table
        arrow_marker.color.r = 1.0f;
        arrow_marker.color.g = 1.0f;
        arrow_marker.color.b = 0.0f;
        arrow_marker.color.a = 1.0f;
        arrow_marker.lifetime = ros::Duration(0.1);

        arrow_marker_array.markers.push_back(arrow_marker);
    }
}


//function for creating objects
void LeapRobotInteraction::createObjects(std::vector<geometry_msgs::Point> objectsOrigins,
                                         std::vector<geometry_msgs::Point> objectsSizes) {

    //int index = 1;
    for (int i = 0, j; i < objectsOrigins.size(); i++) {
        std::string id;
        id = std::to_string(i + 1);
        gObjectOrigin = objectsOrigins.at(i);
        gObjectSize = objectsSizes.at(i);
        //create object
        moveit_msgs::CollisionObject collision_object;
        collision_object.header.frame_id = move_group->getPlanningFrame();
        collision_object.id = id;


        shape_msgs::SolidPrimitive primitive;
        primitive.type = primitive.BOX;
        primitive.dimensions.resize(3);
        primitive.dimensions[0] = gObjectSize.x;
        primitive.dimensions[1] = gObjectSize.y;
        primitive.dimensions[2] = gObjectSize.z;

        geometry_msgs::Pose box_pose;
        box_pose.orientation.w = 1.0;
        box_pose.position.x = gObjectOrigin.x;
        box_pose.position.y = gObjectOrigin.y;
        box_pose.position.z = gObjectOrigin.z + gObjectSize.z / 2;

        collision_object.primitives.push_back(primitive);
        collision_object.primitive_poses.push_back(box_pose);
        collision_object.operation = collision_object.ADD;
        object_ids.push_back(collision_object.id);


        collision_objects.push_back(collision_object);
        ROS_INFO_NAMED("tutorial", "Add an object into the world");
        planning_scene_interface.addCollisionObjects(collision_objects);
        // index++;
    }
}

int LeapRobotInteraction::gesture() {
    double offsetL = 0.01;
    double offsetH = 0.02;

    double distThumb = palm.distanceTo(thumbTip);
    double distIndex = palm.distanceTo(indexTip);
    double distMiddle = palm.distanceTo(middleTip);
    double distRing = palm.distanceTo(ringTip);
    double distPinky = palm.distanceTo(pinkyTip);

    if (isCalibrated) {

        //pointing gesture
        if ((distThumb + offsetH >= userFistCalib.at(0) && distThumb - offsetH <= userFistCalib.at(0)) &&
            (distIndex + offsetL >= userPalmCalib.at(1) && distIndex - offsetL <= userPalmCalib.at(1)) &&
            (distMiddle + offsetH >= userFistCalib.at(2) && distMiddle - offsetH <= userFistCalib.at(2)) &&
            (distRing + offsetH >= userFistCalib.at(3) && distRing - offsetH <= userFistCalib.at(3)) &&
            (distPinky + offsetH >= userFistCalib.at(4) && distPinky - offsetH <= userFistCalib.at(4))
                ) {
           // std::cout << "pointing" << std::endl;
            return 1;
        }
            //pointing gestre with thumb up
        else if ((distThumb + offsetL >= userPalmCalib.at(0) && distThumb - offsetL <= userPalmCalib.at(0)) &&
                 (distIndex + offsetL >= userPalmCalib.at(1) && distIndex - offsetL <= userPalmCalib.at(1)) &&
                 (distMiddle + offsetH >= userFistCalib.at(2) && distMiddle - offsetH <= userFistCalib.at(2)) &&
                 (distRing + offsetH >= userFistCalib.at(3) && distRing - offsetH <= userFistCalib.at(3)) &&
                 (distPinky + offsetH >= userFistCalib.at(4) && distPinky - offsetH <= userFistCalib.at(4))
                ) {
           // std::cout << "pointing to move" << std::endl;
            return 2;
        }
            //thumb up gesture
        else if ((distThumb + offsetL >= userPalmCalib.at(0) && distThumb - offsetL <= userPalmCalib.at(0)) &&
                 (distIndex + offsetH >= userFistCalib.at(1) && distIndex - offsetH <= userFistCalib.at(1)) &&
                 (distMiddle + offsetH >= userFistCalib.at(2) && distMiddle - offsetH <= userFistCalib.at(2)) &&
                 (distRing + offsetH >= userFistCalib.at(3) && distRing - offsetH <= userFistCalib.at(3))
                ) {
            //std::cout << "thumb up" << std::endl;
            return 4;

            //Rocker gesture - home gesture
        } else if ((distThumb + offsetH >= userFistCalib.at(0) && distThumb - offsetH <= userFistCalib.at(0)) &&
                   (distIndex + offsetL >= userPalmCalib.at(1) && distIndex - offsetL <= userPalmCalib.at(1)) &&
                   (distMiddle + offsetH >= userFistCalib.at(2) && distMiddle - offsetH <= userFistCalib.at(2)) &&
                   (distRing + offsetH >= userFistCalib.at(3) && distRing - offsetH <= userFistCalib.at(3)) &&
                   (distPinky + offsetH >= userPalmCalib.at(4) && distPinky - offsetH <= userPalmCalib.at(4))
                ) {
            //std::cout << "go home - calibrated" << std::endl;
            return 5;
            //Pinky Up gesture
        } else if((distThumb + offsetH >= userFistCalib.at(0) && distThumb - offsetH <= userFistCalib.at(0)) &&
                  (distIndex + offsetH >= userFistCalib.at(1) && distIndex - offsetH <= userFistCalib.at(1)) &&
                  (distMiddle + offsetH >= userFistCalib.at(2) && distMiddle - offsetH <= userFistCalib.at(2)) &&
                  (distRing + offsetH >= userFistCalib.at(3) && distRing - offsetH <= userFistCalib.at(3)) &&
                  (distPinky + offsetH >= userPalmCalib.at(4) && distPinky - offsetH <= userPalmCalib.at(4))
                )

        {
            return 3;
        }
    } else {
        //pointing
        if (palm.distanceTo(indexTip) > palm.distanceTo(thumbTip) &&
            palm.distanceTo(indexTip) > 1.5 * palm.distanceTo(ringTip) &&
            palm.distanceTo(indexTip) > 2 * palm.distanceTo(pinkyTip)
                ) {
//            std::cout << "pointing" << std::endl;
            return 1;
        }
        //pointing to move
        if (palm.distanceTo(indexTip) > 1.5 * palm.distanceTo(ringTip) &&
            palm.distanceTo(thumbTip) > 2.275 * palm.distanceTo(ringTip)) {
//            std::cout << "pointing to move" << std::endl;
            return 2;

        }
        //rocker
        if (palm.distanceTo(indexTip) > palm.distanceTo(thumbTip) &&
            palm.distanceTo(indexTip) > 1.5 * palm.distanceTo(ringTip) &&
            palm.distanceTo(pinkyTip) > 1.5 * palm.distanceTo(ringTip)) {
            //std::cout << "go home" << std::endl;
            return 5;

        }
        //pinky up
        if (palm.distanceTo(pinkyTip) > 1.5 * palm.distanceTo(ringTip) &&
            palm.distanceTo(pinkyTip) > 1.25 * palm.distanceTo(indexTip)){
            return 3;

        }
        //thumb up
        if (palm.distanceTo(thumbTip) > 1.5 * palm.distanceTo(ringTip) &&
            palm.distanceTo(thumbTip) > 2 * palm.distanceTo(indexTip)) {
//            std::cout << "go home" << std::endl;
            return 4;
        } else return 0;

    }

}


//function handling the data from leap motion sensor
void LeapRobotInteraction::frameCallback(const leap_motion::Human::ConstPtr &human) {

    leap_motion::Hand hand;
    tf::StampedTransform transform;
    if (human->right_hand.is_present) {
        hand = human->right_hand;
        userRightHand = hand;
        LeapRobotInteraction::fingerPointInit(human, hand);

    }

    //functionality is programed for left hand
    if (human->left_hand.is_present) {
        hand = human->left_hand;
        userLeftHand = hand;
        LeapRobotInteraction::fingerPointInit(human, hand);
    }
}
